// This file has been auto generated, do not edit directly
// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import proguard.gradle.ProGuardTask

group = "org.sapota.marek.kotlin.gpio"

repositories {
    jcenter()
    mavenCentral()
}

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath("com.guardsquare:proguard-gradle:7.0.1")
    }
}

plugins {
    kotlin("jvm") version "1.4.31"
    id("org.jlleitschuh.gradle.ktlint") version "10.0.0"
    // Test coverage
    id("jacoco")
    // Gradle check for version updates
    id("com.github.ben-manes.versions") version "0.36.0"
}

ktlint {
    version.set("0.40.0")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.31")
    implementation("com.github.ajalt.clikt:clikt:3.1.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")
}

sourceSets {
    main {
        java.srcDir("src/main/kotlin/")
    }
}

val dep_classes = configurations.runtimeClasspath.get().map {
    if (it.isDirectory) it else zipTree(it)
}

val jar: Jar by tasks
jar.enabled = true

tasks {
    val gpio_bin_jar by creating(Jar::class) {
        archiveFileName.value("gpio-bin.jar")
        manifest {
            attributes["Main-Class"] = "org.sapota.marek.kotlin.gpio.bin.GpioKt"
        }
        // Include class files.
        from(sourceSets.getByName("main").output)
        // Include classes from dependencies.
        from(dep_classes)
    }
    val gpio_bin_jar_proguard by creating(ProGuardTask::class) {
        dependsOn(gpio_bin_jar)
        verbose()
        dontobfuscate()
        injars("build/libs/gpio-bin.jar")
        outjars("build/libs/gpio-bin.stripped.jar")
        // Define standard library
        listOf(
            "base",
            "desktop",
            "logging",
            "management",
            "scripting",
            "sql",
            "xml"
        ).forEach {
            libraryjars(
                // Duplicate class errors without this
                mapOf(
                    "jarfilter" to "!**.jar",
                    "filter" to "!module-info.class"
                ),
                "${System.getProperty("java.home")}/jmods/java.$it.jmod"
            )
        }
        // Specify class and method that is the entry point and should not be stripped
        keep(
            """class org.sapota.marek.kotlin.gpio.bin.GpioKt {
                public static void main(java.lang.String[]);
            }
            """
        )
        // Should be fixed in future versions
        // https://github.com/Kotlin/kotlinx.coroutines/issues/2046
        dontwarn("java.lang.instrument.ClassFileTransformer")
        dontwarn("sun.misc.SignalHandler")
        dontwarn("java.lang.instrument.Instrumentation")
        dontwarn("sun.misc.Signal")
        dontwarn("kotlin.time.Duration\$Companion")
        // Not mentioned in the issue, probably/hopefully also safe to ignore
        dontwarn("android.annotation.SuppressLint")
    }
    gpio_bin_jar_proguard.enabled = JavaVersion.current() != JavaVersion.VERSION_15
    val gpio_example_sequence_jar by creating(Jar::class) {
        archiveFileName.value("gpio-example-sequence.jar")
        manifest {
            attributes["Main-Class"] = "org.sapota.marek.kotlin.gpio.examples.sequence.bin.SequenceKt"
        }
        // Include class files.
        from(sourceSets.getByName("main").output)
        // Include classes from dependencies.
        from(dep_classes)
    }
    val gpio_example_sequence_jar_proguard by creating(ProGuardTask::class) {
        dependsOn(gpio_example_sequence_jar)
        verbose()
        dontobfuscate()
        injars("build/libs/gpio-example-sequence.jar")
        outjars("build/libs/gpio-example-sequence.stripped.jar")
        // Define standard library
        listOf(
            "base",
            "desktop",
            "logging",
            "management",
            "scripting",
            "sql",
            "xml"
        ).forEach {
            libraryjars(
                // Duplicate class errors without this
                mapOf(
                    "jarfilter" to "!**.jar",
                    "filter" to "!module-info.class"
                ),
                "${System.getProperty("java.home")}/jmods/java.$it.jmod"
            )
        }
        // Specify class and method that is the entry point and should not be stripped
        keep(
            """class org.sapota.marek.kotlin.gpio.examples.sequence.bin.SequenceKt {
                public static void main(java.lang.String[]);
            }
            """
        )
        // Should be fixed in future versions
        // https://github.com/Kotlin/kotlinx.coroutines/issues/2046
        dontwarn("java.lang.instrument.ClassFileTransformer")
        dontwarn("sun.misc.SignalHandler")
        dontwarn("java.lang.instrument.Instrumentation")
        dontwarn("sun.misc.Signal")
        dontwarn("kotlin.time.Duration\$Companion")
        // Not mentioned in the issue, probably/hopefully also safe to ignore
        dontwarn("android.annotation.SuppressLint")
    }
    gpio_example_sequence_jar_proguard.enabled = JavaVersion.current() != JavaVersion.VERSION_15
    val gpio_example_pulse_jar by creating(Jar::class) {
        archiveFileName.value("gpio-example-pulse.jar")
        manifest {
            attributes["Main-Class"] = "org.sapota.marek.kotlin.gpio.examples.pulse.bin.PulseKt"
        }
        // Include class files.
        from(sourceSets.getByName("main").output)
        // Include classes from dependencies.
        from(dep_classes)
    }
    val gpio_example_pulse_jar_proguard by creating(ProGuardTask::class) {
        dependsOn(gpio_example_pulse_jar)
        verbose()
        dontobfuscate()
        injars("build/libs/gpio-example-pulse.jar")
        outjars("build/libs/gpio-example-pulse.stripped.jar")
        // Define standard library
        listOf(
            "base",
            "desktop",
            "logging",
            "management",
            "scripting",
            "sql",
            "xml"
        ).forEach {
            libraryjars(
                // Duplicate class errors without this
                mapOf(
                    "jarfilter" to "!**.jar",
                    "filter" to "!module-info.class"
                ),
                "${System.getProperty("java.home")}/jmods/java.$it.jmod"
            )
        }
        // Specify class and method that is the entry point and should not be stripped
        keep(
            """class org.sapota.marek.kotlin.gpio.examples.pulse.bin.PulseKt {
                public static void main(java.lang.String[]);
            }
            """
        )
        // Should be fixed in future versions
        // https://github.com/Kotlin/kotlinx.coroutines/issues/2046
        dontwarn("java.lang.instrument.ClassFileTransformer")
        dontwarn("sun.misc.SignalHandler")
        dontwarn("java.lang.instrument.Instrumentation")
        dontwarn("sun.misc.Signal")
        dontwarn("kotlin.time.Duration\$Companion")
        // Not mentioned in the issue, probably/hopefully also safe to ignore
        dontwarn("android.annotation.SuppressLint")
    }
    gpio_example_pulse_jar_proguard.enabled = JavaVersion.current() != JavaVersion.VERSION_15
    val gpio_example_cpufreq_jar by creating(Jar::class) {
        archiveFileName.value("gpio-example-cpufreq.jar")
        manifest {
            attributes["Main-Class"] = "org.sapota.marek.kotlin.gpio.examples.cpufreq.bin.CpuFreqKt"
        }
        // Include class files.
        from(sourceSets.getByName("main").output)
        // Include classes from dependencies.
        from(dep_classes)
    }
    val gpio_example_cpufreq_jar_proguard by creating(ProGuardTask::class) {
        dependsOn(gpio_example_cpufreq_jar)
        verbose()
        dontobfuscate()
        injars("build/libs/gpio-example-cpufreq.jar")
        outjars("build/libs/gpio-example-cpufreq.stripped.jar")
        // Define standard library
        listOf(
            "base",
            "desktop",
            "logging",
            "management",
            "scripting",
            "sql",
            "xml"
        ).forEach {
            libraryjars(
                // Duplicate class errors without this
                mapOf(
                    "jarfilter" to "!**.jar",
                    "filter" to "!module-info.class"
                ),
                "${System.getProperty("java.home")}/jmods/java.$it.jmod"
            )
        }
        // Specify class and method that is the entry point and should not be stripped
        keep(
            """class org.sapota.marek.kotlin.gpio.examples.cpufreq.bin.CpuFreqKt {
                public static void main(java.lang.String[]);
            }
            """
        )
        // Should be fixed in future versions
        // https://github.com/Kotlin/kotlinx.coroutines/issues/2046
        dontwarn("java.lang.instrument.ClassFileTransformer")
        dontwarn("sun.misc.SignalHandler")
        dontwarn("java.lang.instrument.Instrumentation")
        dontwarn("sun.misc.Signal")
        dontwarn("kotlin.time.Duration\$Companion")
        // Not mentioned in the issue, probably/hopefully also safe to ignore
        dontwarn("android.annotation.SuppressLint")
    }
    gpio_example_cpufreq_jar_proguard.enabled = JavaVersion.current() != JavaVersion.VERSION_15
    val gpio_example_cputemp_jar by creating(Jar::class) {
        archiveFileName.value("gpio-example-cputemp.jar")
        manifest {
            attributes["Main-Class"] = "org.sapota.marek.kotlin.gpio.examples.cputemp.bin.CpuTempKt"
        }
        // Include class files.
        from(sourceSets.getByName("main").output)
        // Include classes from dependencies.
        from(dep_classes)
    }
    val gpio_example_cputemp_jar_proguard by creating(ProGuardTask::class) {
        dependsOn(gpio_example_cputemp_jar)
        verbose()
        dontobfuscate()
        injars("build/libs/gpio-example-cputemp.jar")
        outjars("build/libs/gpio-example-cputemp.stripped.jar")
        // Define standard library
        listOf(
            "base",
            "desktop",
            "logging",
            "management",
            "scripting",
            "sql",
            "xml"
        ).forEach {
            libraryjars(
                // Duplicate class errors without this
                mapOf(
                    "jarfilter" to "!**.jar",
                    "filter" to "!module-info.class"
                ),
                "${System.getProperty("java.home")}/jmods/java.$it.jmod"
            )
        }
        // Specify class and method that is the entry point and should not be stripped
        keep(
            """class org.sapota.marek.kotlin.gpio.examples.cputemp.bin.CpuTempKt {
                public static void main(java.lang.String[]);
            }
            """
        )
        // Should be fixed in future versions
        // https://github.com/Kotlin/kotlinx.coroutines/issues/2046
        dontwarn("java.lang.instrument.ClassFileTransformer")
        dontwarn("sun.misc.SignalHandler")
        dontwarn("java.lang.instrument.Instrumentation")
        dontwarn("sun.misc.Signal")
        dontwarn("kotlin.time.Duration\$Companion")
        // Not mentioned in the issue, probably/hopefully also safe to ignore
        dontwarn("android.annotation.SuppressLint")
    }
    gpio_example_cputemp_jar_proguard.enabled = JavaVersion.current() != JavaVersion.VERSION_15
    withType<Jar> {
        duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    }

    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.time.ExperimentalTime"
    }

    val proguard by creating(DefaultTask::class) {
        dependsOn("gpio_bin_jar_proguard")
        dependsOn("gpio_example_sequence_jar_proguard")
        dependsOn("gpio_example_pulse_jar_proguard")
        dependsOn("gpio_example_cpufreq_jar_proguard")
        dependsOn("gpio_example_cputemp_jar_proguard")
    }

    withType<Test> {
        useJUnitPlatform()
        finalizedBy("jacocoTestReport")
    }

    withType<DependencyUpdatesTask> {
        checkForGradleUpdate = false
        outputDir = "build/reports/dependencyUpdates"
    }

    named("dependencyUpdates", DependencyUpdatesTask::class.java).configure {
        // Ignore non-stable versions.
        rejectVersionIf {
            candidate.version.contains("-RC") ||
                ".*-M[0-9]".toRegex().matches(candidate.version) ||
                ".*-beta[0-9]".toRegex().matches(candidate.version)
        }
    }

    assemble {
        dependsOn("gpio_bin_jar")
        dependsOn("gpio_example_sequence_jar")
        dependsOn("gpio_example_pulse_jar")
        dependsOn("gpio_example_cpufreq_jar")
        dependsOn("gpio_example_cputemp_jar")
    }
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = true
        csv.isEnabled = false
        html.isEnabled = true
        html.destination = file("$buildDir/reports/jacoco/")
    }
}
