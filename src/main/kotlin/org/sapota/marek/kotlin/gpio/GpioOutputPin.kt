// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio

import org.sapota.marek.kotlin.gpio.base.GpioPin

open class GpioOutputPin(number: Int) : GpioPin(number) {
    fun export() {
        super.export(GpioDirection.OUT)
    }

    override fun unexportCleanup() {
        super.unexportCleanup()
        setValue(GpioLevel.LOW)
    }

    fun setValue(level: GpioLevel) {
        export(GpioDirection.OUT)
        val levelString = level.value.toString()
        val value = getValueFile()
        value.seek(0)
        value.writeBytes(levelString)
        value.setLength(levelString.length.toLong())
    }
}
