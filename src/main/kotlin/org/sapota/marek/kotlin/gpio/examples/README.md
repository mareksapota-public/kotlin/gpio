<!-- MIT Expat License

Copyright 2021 Marek Sapota

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. -->

# Single 74hc595 and 8 leds

## Wiring diagram

```
ground
|
+-- 330Ω resistor -- LED 1 -------------------------+
+-- 330Ω resistor -- LED 2 --┌─┐─┌─┐-- power 3.3V   |
+-- 330Ω resistor -- LED 3 --│ ╰─╯ │----------------+
+-- 330Ω resistor -- LED 4 --│     │-- data -----------┌────┐
+-- 330Ω resistor -- LED 5 --│74hc5│-- ground          │    │
+-- 330Ω resistor -- LED 6 --│ 95  │-- latch ----------│GPIO│
+-- 330Ω resistor -- LED 7 --│     │-- clock ----------│    │
+-- 330Ω resistor -- LED 8 --│     │-- power 3.3V      └────┘
                    ground --└─────┘--
```

## Usage

Light up LED 1 and rotate which LED is lit up.

```shell
java -jar gpio-example-sequence.jar "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
```

Pulse/wave animation.

```shell
java -jar gpio-example-pulse.jar "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
# 💡 💡 🔅 🔅 🔅 🔅 💡 💡
# ----->           <-----
# 🔅 💡 💡 🔅 🔅 💡 💡 🔅
#    ----->     <-----
#          .....
# 🔅 🔅 🔅 💡 💡 🔅 🔅 🔅
#          vvvvv
# 🔅 🔅 🔅 🔅 🔅 🔅 🔅 🔅
#          .....
# 💡 🔅 🔅 🔅 🔅 🔅 🔅 💡
# -->                 <--
# 💡 💡 🔅 🔅 🔅 🔅 💡 💡
```

Display current CPU frequency or temperature by ligting up more LEDs as values
get higher.

```shell
java -jar gpio-example-cpufreq.jar "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
java -jar gpio-example-cputemp.jar "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
```

# Two 74hc595 modules and 16 leds

## Wiring diagram

Clock and latch of both modules should be connected to the same GPIO pins.

```
ground
|
+-- 330Ω resistor -- LED 1  -------------------------+
+-- 330Ω resistor -- LED 2  --┌─┐─┌─┐-- power 3.3V   |
+-- 330Ω resistor -- LED 3  --│ ╰─╯ │----------------+
+-- 330Ω resistor -- LED 4  --│     │-- data -----------------┌────┐
+-- 330Ω resistor -- LED 5  --│74hc5│-- ground                │    │
+-- 330Ω resistor -- LED 6  --│ 95  │-- latch -----------+----│GPIO│
+-- 330Ω resistor -- LED 7  --│     │-- clock ----------=|=+--│    │
+-- 330Ω resistor -- LED 8  --│     │-- power 3.3V       | |  └────┘
|                   ground  --└─────┘------------------+ | |
|                                                      | | |
+-- 330Ω resistor -- LED 9  -------------------------+ | | |
+-- 330Ω resistor -- LED 10 --┌─┐─┌─┐-- power 3.3V   | | | |
+-- 330Ω resistor -- LED 11 --│ ╰─╯ │----------------+ | | |
+-- 330Ω resistor -- LED 12 --│     │-- data ----------+ | |
+-- 330Ω resistor -- LED 13 --│74hc5│-- ground           | |
+-- 330Ω resistor -- LED 14 --│ 95  │-- latch -----------+ |
+-- 330Ω resistor -- LED 15 --│     │-- clock -------------+
+-- 330Ω resistor -- LED 16 --│     │-- power 3.3V
                    ground  --└─────┘--

Legend:
   |
 -=|=- Bridge, wires not connected
   |

   |
 --+-- Wires connected
   |
```

## Usage

```shell
java -jar gpio-example-sequence.jar --daisy-chain-length 2 "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
java -jar gpio-example-pulse.jar --daisy-chain-length 2 "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
java -jar gpio-example-cpufreq.jar --daisy-chain-length 2 "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
java -jar gpio-example-cputemp.jar --daisy-chain-length 2 "${DATA_PIN}" "${CLOCK_PIN}" "${LATCH_PIN}"
```
