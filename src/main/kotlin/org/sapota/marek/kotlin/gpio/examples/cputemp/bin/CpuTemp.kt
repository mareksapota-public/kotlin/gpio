// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio.examples.cputemp.bin

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.double
import org.sapota.marek.kotlin.gpio.clikt.daisyChainLengthOption
import org.sapota.marek.kotlin.gpio.clikt.pinArgument
import org.sapota.marek.kotlin.gpio.examples.cputemp.CpuTemp74hc595

class CpuTempBin : CliktCommand() {
    val defaultUpdateFrequency = 0.5

    val updateFrequency: Double by option(
        help = "Delay between updates, defaults to %.1f"
            .format(defaultUpdateFrequency)
    ).double().default(defaultUpdateFrequency)

    val dataPin: Int by pinArgument()
    val clockPin: Int by pinArgument()
    val latchPin: Int by pinArgument()

    val daisyChainLength: Int by daisyChainLengthOption()

    override fun run() {
        CpuTemp74hc595(
            dataPin,
            clockPin,
            latchPin,
            daisyChainLength,
            updateFrequency,
        ).run()
    }
}

fun main(args: Array<String>) = CpuTempBin().main(args)
