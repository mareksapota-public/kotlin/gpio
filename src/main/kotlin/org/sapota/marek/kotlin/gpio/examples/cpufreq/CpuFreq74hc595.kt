// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio.examples.cpufreq

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.sapota.marek.kotlin.gpio.Gpio74hc595
import org.sapota.marek.kotlin.gpio.GpioLevel
import org.sapota.marek.kotlin.gpio.examples.common.PinsFor74hc595
import java.io.File
import kotlin.math.min
import kotlin.time.DurationUnit
import kotlin.time.toDuration

enum class CpuFreqType {
    MIN,
    CUR,
    MAX,
}

class CpuFreq74hc595(
    val dataPin: Int,
    val clockPin: Int,
    val latchPin: Int,
    val daisyChainLength: Int,
    val updateFrequencySeconds: Double,
) {
    val freqCache: MutableMap<CpuFreqType, Int> = mutableMapOf()

    fun readFreq(type: CpuFreqType, cache: Boolean = false): Int {
        if (cache && freqCache.contains(type)) {
            return freqCache.get(type)!!
        }
        val path = "${CpuFreq74hc595.sysBasePath}/" +
            "scaling_${type.name.toLowerCase()}_freq"
        val freq = File(path).readText().trim().toInt()
        if (cache) {
            freqCache.put(type, freq)
        }
        return freq
    }

    fun minCpuFreq(): Int {
        return readFreq(CpuFreqType.MIN, cache = true)
    }

    fun maxCpuFreq(): Int {
        return readFreq(CpuFreqType.MAX, cache = true)
    }

    fun curCpuFreq(): Int {
        return readFreq(CpuFreqType.CUR)
    }

    fun numHighPins(): Int {
        val numPins = daisyChainLength * 8
        val freqRange = maxCpuFreq() - minCpuFreq()
        val step = freqRange / numPins
        val curFreqAdjusted = curCpuFreq() - minCpuFreq()
        return min(numPins, 1 + curFreqAdjusted / step)
    }

    fun run() = runBlocking(Dispatchers.Default) {
        val delayDuration = updateFrequencySeconds
            .toDuration(DurationUnit.SECONDS)
        val pins = PinsFor74hc595.get(dataPin, clockPin, latchPin)
        val controller = Gpio74hc595(
            pins.dataPin,
            pins.clockPin,
            pins.latchPin,
            daisyChainLength = daisyChainLength,
        )
        while (true) {
            val levels = (1..numHighPins()).map {
                GpioLevel.HIGH
            }
            controller.setBits(levels)
            delay(delayDuration)
        }
    }

    companion object {
        val sysBasePath = "/sys/devices/system/cpu/cpu0/cpufreq"
    }
}
