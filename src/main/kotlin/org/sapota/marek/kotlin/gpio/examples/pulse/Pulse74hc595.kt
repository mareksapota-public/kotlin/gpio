// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio.examples.pulse

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.sapota.marek.kotlin.gpio.Gpio74hc595
import org.sapota.marek.kotlin.gpio.GpioLevel
import org.sapota.marek.kotlin.gpio.examples.common.PinsFor74hc595
import java.util.Collections
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class Pulse74hc595(
    val dataPin: Int,
    val clockPin: Int,
    val latchPin: Int,
    val daisyChainLength: Int,
    val delaySeconds: Double,
) {
    fun run() = runBlocking(Dispatchers.Default) {
        val delayDuration = delaySeconds.toDuration(DurationUnit.SECONDS)
        val pins = PinsFor74hc595.get(dataPin, clockPin, latchPin)
        val controller = Gpio74hc595(
            pins.dataPin,
            pins.clockPin,
            pins.latchPin,
            daisyChainLength = daisyChainLength,
        )
        val numLeds = 8 * daisyChainLength
        val litLeds = 2 * daisyChainLength
        val levels = (1..litLeds).map { GpioLevel.HIGH }.plus(
            (1..(numLeds - litLeds)).map { GpioLevel.LOW },
        )
        var counter = 0
        while (true) {
            val halfLevels = levels.take(levels.size / 2)
            val fullLevels = halfLevels.plus(halfLevels.reversed())
            controller.setBits(fullLevels)
            Collections.rotate(levels, 1)
            ++counter
            delay(delayDuration)
        }
    }
}
