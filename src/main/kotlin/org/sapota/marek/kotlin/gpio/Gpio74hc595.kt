// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio

import kotlinx.coroutines.delay
import kotlin.math.ceil
import kotlin.math.pow
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

open class Gpio74hc595(
    val dataPin: GpioOutputPin,
    val clockPin: GpioOutputPin,
    val latchPin: GpioOutputPin,
    // 8Mhz should be achievable on 3.3V
    val clockSpeed: Int = 8000000,
    val daisyChainLength: Int = 1,
) {
    val clockSleep: Duration = ceil(10.0.pow(9) / clockSpeed)
        .toDuration(DurationUnit.NANOSECONDS)

    suspend fun clockDelay() {
        delay(clockSleep)
    }

    suspend fun addBit(level: GpioLevel) {
        dataPin.setValue(level)
        clockPin.setValue(GpioLevel.HIGH)
        clockDelay()
        clockPin.setValue(GpioLevel.LOW)
    }

    suspend fun addBits(levels: Iterable<GpioLevel>) {
        levels.reversed().forEach {
            addBit(it)
        }
    }

    suspend fun latch() {
        latchPin.setValue(GpioLevel.HIGH)
        clockDelay()
        latchPin.setValue(GpioLevel.LOW)
    }

    suspend fun setBits(levels: Iterable<GpioLevel>) {
        var offset = 0
        while ((levels.count() + offset) != 8 * daisyChainLength) {
            addBit(GpioLevel.LOW)
            ++offset
        }
        addBits(levels)
        latch()
    }
}
