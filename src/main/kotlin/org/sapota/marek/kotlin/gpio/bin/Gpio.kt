// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio.bin

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.convert
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.types.enum
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.sapota.marek.kotlin.gpio.Gpio74hc595
import org.sapota.marek.kotlin.gpio.GpioDirection
import org.sapota.marek.kotlin.gpio.GpioInputPin
import org.sapota.marek.kotlin.gpio.GpioLevel
import org.sapota.marek.kotlin.gpio.GpioOutputPin
import org.sapota.marek.kotlin.gpio.base.GpioPin
import org.sapota.marek.kotlin.gpio.clikt.daisyChainLengthOption
import org.sapota.marek.kotlin.gpio.clikt.pinArgument

enum class GpioAction {
    EXPORT,
    UNEXPORT,
    WRITE,
    READ,
    GPIO74HC595;

    val value: String = name.toLowerCase()

    companion object {
        val help: String =
            GpioAction.values().map { it.value }.joinToString("/")
    }
}

fun CliktCommand.pinLevelArgument() = argument(
    help = "Level to set the pin to (0/low or 1/high)"
).convert {
    when (it) {
        "0" -> GpioLevel.LOW.name
        "1" -> GpioLevel.HIGH.name
        else -> it
    }
}.enum<GpioLevel>()

class GpioBinExport : CliktCommand() {
    val pinNumber: Int by pinArgument()
    val direction: GpioDirection by argument(
        help = "Configure the pin as input/output",
    ).enum<GpioDirection>()

    override fun run() {
        when (direction) {
            GpioDirection.IN -> GpioInputPin(pinNumber).export()
            GpioDirection.OUT -> GpioOutputPin(pinNumber).export()
        }
    }
}

class GpioBinUnexport : CliktCommand() {
    val pinNumber: Int by pinArgument()

    override fun run() = runBlocking(Dispatchers.Default) {
        GpioPin.loadExisting(pinNumber).unexport()
    }
}

class GpioBinWrite : CliktCommand() {
    val pinNumber: Int by pinArgument()
    val level: GpioLevel by pinLevelArgument()

    override fun run() = runBlocking(Dispatchers.Default) {
        GpioOutputPin(pinNumber).setValue(level)
    }
}

class GpioBinRead : CliktCommand() {
    val pinNumber: Int by pinArgument()

    override fun run() {
        val value = GpioInputPin(pinNumber).getValue()
        println(value)
    }
}

class GpioBin74hc595 : CliktCommand() {
    val dataPin: Int by pinArgument()
    val clockPin: Int by pinArgument()
    val latchPin: Int by pinArgument()
    val daisyChainLength: Int by daisyChainLengthOption()
    val levels: List<GpioLevel> by pinLevelArgument().multiple()

    override fun run() = runBlocking(Dispatchers.Default) {
        Gpio74hc595(
            GpioOutputPin(dataPin),
            GpioOutputPin(clockPin),
            GpioOutputPin(latchPin),
            daisyChainLength = daisyChainLength,
        ).setBits(levels)
    }
}

class GpioBin : CliktCommand() {
    val action: GpioAction by argument(
        help = GpioAction.help,
    ).enum<GpioAction>()

    val args: List<String> by argument(
        help = "Action arguments, use `<ACTION> -- --help` to see action " +
            "specific help",
    ).multiple()

    override fun run() {
        when (action) {
            GpioAction.EXPORT -> GpioBinExport().main(args)
            GpioAction.UNEXPORT -> GpioBinUnexport().main(args)
            GpioAction.WRITE -> GpioBinWrite().main(args)
            GpioAction.READ -> GpioBinRead().main(args)
            GpioAction.GPIO74HC595 -> GpioBin74hc595().main(args)
        }
    }
}

fun main(args: Array<String>) = GpioBin().main(args)
