// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio.base

import org.sapota.marek.kotlin.gpio.GpioDirection
import org.sapota.marek.kotlin.gpio.GpioInputPin
import org.sapota.marek.kotlin.gpio.GpioOutputPin
import org.sapota.marek.kotlin.gpio.exceptions.GpioDirectionException
import org.sapota.marek.kotlin.gpio.exceptions.GpioNotExportedException
import java.io.File
import java.io.RandomAccessFile

open class GpioPin protected constructor (val number: Int) {
    val sysPath: String = "${GpioPin.sysBasePath}/gpio$number"
    val valuePath: String = "$sysPath/value"
    val directionPath: String = "$sysPath/direction"

    private var isExportedCache: Boolean? = null
    private var directionCache: GpioDirection? = null
    private var valueFile: RandomAccessFile? = null

    fun isExported(): Boolean {
        if (isExportedCache == null) {
            isExportedCache = File(sysPath).exists()
        }
        return isExportedCache!!
    }

    fun export(direction: GpioDirection) {
        if (isExported()) {
            if (this.direction != direction) {
                throw GpioDirectionException(
                    "Pin $number already exported as an " +
                        "${this.direction.name} pin",
                )
            }
        } else {
            File(GpioPin.exportPath).writeText(number.toString())
            File(directionPath).writeText(direction.value)
            isExportedCache = true
            directionCache = direction
        }
    }

    fun unexport() {
        if (isExported()) {
            unexportCleanup()
            File(GpioPin.unexportPath).writeText(number.toString())
            isExportedCache = false
            directionCache = null
        }
    }

    protected open fun unexportCleanup() {
        valueFile?.close()
        valueFile = null
    }

    protected fun getValueFile(): RandomAccessFile {
        if (valueFile == null) {
            if (!isExported()) {
                throw GpioNotExportedException(
                    "Can not open value file of unexported GPIO pin $number"
                )
            }
            valueFile = RandomAccessFile(valuePath, "rwd")
        }
        return valueFile!!
    }

    val direction: GpioDirection
        get() {
            if (!isExported()) {
                throw GpioNotExportedException(
                    "Can not read direction of unexported GPIO pin $number"
                )
            }
            if (directionCache == null) {
                directionCache = GpioDirection.valueOf(
                    File(directionPath).readText().trim().toUpperCase()
                )
            }
            return directionCache!!
        }

    companion object {
        val sysBasePath = "/sys/class/gpio"
        val exportPath = "$sysBasePath/export"
        val unexportPath = "$sysBasePath/unexport"

        fun loadExisting(number: Int): GpioPin {
            return when (GpioPin(number).direction) {
                GpioDirection.IN -> GpioInputPin(number)
                GpioDirection.OUT -> GpioOutputPin(number)
            }
        }
    }
}
