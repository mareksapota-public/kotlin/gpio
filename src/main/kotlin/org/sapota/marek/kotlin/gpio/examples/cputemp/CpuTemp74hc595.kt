// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio.examples.cputemp

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.sapota.marek.kotlin.gpio.Gpio74hc595
import org.sapota.marek.kotlin.gpio.GpioLevel
import org.sapota.marek.kotlin.gpio.examples.common.PinsFor74hc595
import java.io.File
import java.lang.RuntimeException
import kotlin.math.min
import kotlin.time.DurationUnit
import kotlin.time.toDuration

enum class CpuTempType(val fileName: String) {
    CUR("input"),
    CRIT("crit"),
    MAX("max"),
    MAX_CUR("max_cur"),
    MIN_CUR("min_cur"),
}

class CpuTemp74hc595(
    val dataPin: Int,
    val clockPin: Int,
    val latchPin: Int,
    val daisyChainLength: Int,
    val updateFrequencySeconds: Double,
) {
    val tempCache = mutableMapOf<CpuTempType, Int?>()
    var hwmonIdCache = -1

    fun hwmonId(): Int {
        if (hwmonIdCache >= 0) {
            return hwmonIdCache
        }
        val hwmonPrefix = "hwmon"
        val candidates = File(sysHwmonPath).listFiles { f: File ->
            f.name.startsWith(hwmonPrefix)
        }.filter {
            setOf<String>(
                "coretemp",
                "soc_thermal",
                "scpi_sensors",
            ).contains(
                File("${it.path}/name").readText().trim()
            )
        }
        if (candidates.isEmpty()) {
            throw RuntimeException("Can not find the CPU hwmon ID")
        }
        hwmonIdCache = candidates.first().name.removePrefix(hwmonPrefix)
            .toInt()
        return hwmonIdCache
    }

    fun readTemp(type: CpuTempType, cache: Boolean = false): Int? {
        if (cache && tempCache.contains(type)) {
            return tempCache.get(type)!!
        }
        val tempFile = File(
            "$sysHwmonPath/hwmon${hwmonId()}/temp1_${type.fileName}"
        )
        val temp = if (tempFile.exists()) {
            tempFile.readText().trim().toInt()
        } else {
            null
        }
        if (cache) {
            tempCache.put(type, temp)
        }
        return temp
    }

    fun curCpuTemp(): Int {
        return readTemp(CpuTempType.CUR)!!
    }

    fun critCpuTemp(): Int? {
        return readTemp(CpuTempType.CRIT)
    }

    fun maxCpuTemp(): Int {
        var maxTemp = readTemp(CpuTempType.MAX) ?: critCpuTemp()
        return if (maxTemp != null) {
            maxTemp
        } else {
            val curTemp = curCpuTemp()
            val maxCurTemp = tempCache.get(CpuTempType.MAX_CUR) ?: Int.MIN_VALUE
            return if (curTemp > maxCurTemp) {
                tempCache.put(CpuTempType.MAX_CUR, curTemp)
                println("↑ Max CPU temperature set to $curTemp")
                curTemp
            } else {
                maxCurTemp
            }
        }
    }

    fun minCpuTemp(): Int {
        val curTemp = curCpuTemp()
        val minCurTemp = tempCache.get(CpuTempType.MIN_CUR) ?: Int.MAX_VALUE
        return if (curTemp < minCurTemp) {
            tempCache.put(CpuTempType.MIN_CUR, curTemp)
            println("↓ Min CPU temperature set to $curTemp")
            curTemp
        } else {
            minCurTemp
        }
    }

    fun numHighPins(): Int {
        val numPins = daisyChainLength * 8
        val minTemp = minCpuTemp()
        val maxTemp = maxCpuTemp()
        val tempRange = maxTemp - minTemp
        if (tempRange == 0) {
            return 1
        }
        val step = tempRange / numPins
        val curTempAdjusted = curCpuTemp() - minTemp
        return min(numPins, 1 + curTempAdjusted / step)
    }

    fun run() = runBlocking(Dispatchers.Default) {
        val delayDuration = updateFrequencySeconds
            .toDuration(DurationUnit.SECONDS)
        val pins = PinsFor74hc595.get(dataPin, clockPin, latchPin)
        val controller = Gpio74hc595(
            pins.dataPin,
            pins.clockPin,
            pins.latchPin,
            daisyChainLength = daisyChainLength,
        )
        while (true) {
            val levels = (1..numHighPins()).map {
                GpioLevel.HIGH
            }
            controller.setBits(levels)
            delay(delayDuration)
        }
    }

    companion object {
        val sysHwmonPath = "/sys/class/hwmon/"
    }
}
