// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.kotlin.gpio.examples.common

import org.sapota.marek.kotlin.gpio.GpioOutputPin
import sun.misc.Signal

data class PinsFor74hc595(
    val dataPin: GpioOutputPin,
    val clockPin: GpioOutputPin,
    val latchPin: GpioOutputPin,
) {
    companion object {
        fun get(
            dataPinNumber: Int,
            clockPinNumber: Int,
            latchPinNumber: Int,
        ): PinsFor74hc595 {
            val gpioData = GpioOutputPin(dataPinNumber)
            val gpioClock = GpioOutputPin(clockPinNumber)
            val gpioLatch = GpioOutputPin(latchPinNumber)
            Signal.handle(Signal("INT")) {
                listOf(gpioData, gpioClock, gpioLatch)
                    .forEach { pin: GpioOutputPin ->
                        pin.unexport()
                    }
                System.exit(1)
            }
            return PinsFor74hc595(gpioData, gpioClock, gpioLatch)
        }
    }
}
