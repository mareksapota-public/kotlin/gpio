<!-- MIT Expat License

Copyright 2021 Marek Sapota

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. -->

# Kotlin GPIO

This library provides a Kotlin interface for GPIO on Linux using sysfs.  Since
the library uses sysfs it should work with any board as long as the kernel can
technically interact with the GPIO interface.  Consult documentation of your
board to find out what sysfs interface numbers correspond to the physical pins.
You're looking for something like
[this](https://wiki.friendlyarm.com/wiki/index.php/NanoPi_NEO3#Layout) or
[this](https://libre.computer/2018/05/19/gpio-headers-reference-for-aml-s905x-cc/).

## Single GPIO pins

Pins are auto exported when used but have to be unexported manually.  Output
pins are set to low level when unexported.

### Output

```kotlin
GpioOutputPin(123).setValue(GpioLevel.HIGH)
GpioOutputPin(123).setValue(GpioLevel.LOW)
```

```console
$ java -jar gpio-bin.jar write -- 123 1
$ java -jar gpio-bin.jar write -- 123 high
$ java -jar gpio-bin.jar write -- 123 0
$ java -jar gpio-bin.jar write -- 123 low
```

### Input

```kotlin
val level = GpioInputPin(123).getValue()
```

```console
$ java -jar gpio-bin.jar read -- 123
HIGH
$ java -jar gpio-bin.jar read -- 123
LOW
```

### Unexport

```kotlin
GpioInputPin(123).unexport()
GpioOutputPin(123).unexport() // auto set to LOW
```

```console
$ java -jar gpio-bin.jar unexport -- 123
```

## 74hc595 shift register

```
              | half circle mark on this side
              v
output 2 --┌─┐─┌─┐-- power (3.3V/5V)
output 3 --│ ╰─╯ │-- output 1
output 4 --│     │-- data
output 5 --│     │-- output enable
output 6 --│     │-- latch
output 7 --│     │-- clock
output 8 --│     │-- clear
  ground --└─────┘-- daisy chain
```

The included 74hc595 shift register class allows easier interaction with these
registers.

```kotlin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking

runBlocking(Dispatchers.Default) {
    val dataPin = GpioOutputPin(dataPinNumber)
    val clockPin = GpioOutputPin(clockPinNumber)
    val latchPin = GpioOutputPin(latchPinNumber)
    val sr = Gpio74hc595(
        dataPin,
        clockPin,
        latchPin,
        // Optional, in hz
        // Realistic speeds this library can achieve through sysfs are 1-2Mhz
        clockSpeed = 8000000, // 8Mhz default
    )
    (0..7).forEach {
        sr.addBit(GpioLevel.HIGH)
    }
    // output 1 2 3 4 5 6 7 8
    // value  1 1 1 1 1 1 1 1
    sr.addBit(GpioLevel.LOW)
    // output 1 2 3 4 5 6 7 8
    // value  0 1 1 1 1 1 1 1
    sr.addBits(listOf(GpioLevel.HIGH, GpioLevel.LOW))
    // output 1 2 3 4 5 6 7 8
    // value  1 0 0 1 1 1 1 1
    sr.latch()
    // output 1 2 3 4 5 6 7 8
    // value  0 1 0 0 0 0 0 0
    // output 3-8 low (default if not enough levels are supplied)
    // sets outputs and latches
    sr.setBits(listOf(GpioLevel.LOW, GpioLevel.HIGH))
    // unexport to ensure pins are set to low
    dataPin.unexport()
    clockPin.unexport()
    latchPin.unexport()
}
```

```console
$ java -jar gpio-bin.jar gpio74hc595 -- 123 124 125 1 0 1 0 1 0 1 0
$ java -jar gpio-bin.jar gpio74hc595 -- 123 124 125 1 1 0 1 # output values 1 1 0 1 0 0 0 0
$ java -jar gpio-bin.jar gpio74hc595 -- 123 124 125 # output values 0 0 0 0 0 0 0 0
```

### Daisy chaining

Multiple 74hc595 modules daisy chained together are also supported.

```kotlin
val sr = Gpio74hc595(dataPin, clockPin, latchPin, daisyChainLength = 2)

sr.setBits(listOf(
    GpioLevel.HIGH,
    GpioLevel.HIGH,
    GpioLevel.HIGH,
    GpioLevel.HIGH,
    GpioLevel.LOW,
    GpioLevel.LOW,
    GpioLevel.LOW,
    GpioLevel.LOW,
    GpioLevel.HIGH,
    GpioLevel.HIGH,
))
// output 1 2 3 4 5 6 7 8 9 a b c d e f g
// value  1 1 1 1 0 0 0 0 1 1 0 0 0 0 0 0
```

```console
$ java -jar gpio-bin.jar gpio74hc595 -- --daisy-chain-length 2 123 124 125 1 1 1 0 1
```

## Examples

Included usage examples and schematics are available
[here](src/main/kotlin/org/sapota/marek/kotlin/gpio/examples/).
